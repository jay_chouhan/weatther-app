import React, { Component } from 'react';

export default class Card extends Component {
  getDayName = (day) => {
    let dayArr = ['Sun', 'Mon', 'Tue', 'Wed', 'Thurs', 'Fri', 'Sat'];
    let dayName = dayArr[day];
    return dayName;
  };

  getDate = (dt) => {
    let date = new Date(dt * 1000);
    let today = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();

    return today;
  };

  getTempInCelcius = (temp) => {
    return Math.floor(temp - 273.15);
  };

  getIcon = (icon) => {
    return `https://openweathermap.org/img/w/${icon}.png`;
  };

  render() {
    return (
      <div className='card'>
        <p>{this.props.getDetails.report.weather[0].description}</p>
        <div className='description'>
          <img className='img' src={this.getIcon(this.props.getDetails.report.weather[0].icon)} alt=''></img>
          <div className='card-title'>
            <h2>{this.getTempInCelcius(this.props.getDetails.report.main.temp)}&#176;C</h2>
          </div>
        </div>

        <h2>{this.getDayName(this.props.getDetails.day)}</h2>
        <h2>{this.getDate(this.props.getDetails.report.dt)}</h2>

        <h3 className='temp'>
          <span>mn:{this.getTempInCelcius(this.props.getDetails.report.main.temp_min)}&#176;C </span>
          <span> mx:{this.getTempInCelcius(this.props.getDetails.report.main.temp_max)}&#176;C</span>
        </h3>
      </div>
    );
  }
}
