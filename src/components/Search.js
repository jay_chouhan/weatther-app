import React, { Component } from 'react';
import ErrorBox from './errorBox';

export default class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      city: '',
      data: null,
      message: '',
    };
  }
  handleChange = (event) => {
    this.setState({ city: event.target.value });
  };

  handleClick = (event) => {
    event.preventDefault();
    let cityRegex = new RegExp('[a-zA-Z][a-zA-Z ]+');
    if (cityRegex.test(this.state.city)) {
      this.props.getData(this.state.city);
      this.setState({ message: this.props.msg });
    } else {
      this.setState({ message: 'Invalid Input' });
    }
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.msg !== this.state.message) {
      this.setState({ message: nextProps.msg });
    }
  }

  render() {
    return (
      <div className='form-container'>
        <form className='form'>
          <input
            type='search'
            className='form-control'
            placeholder='search for a city'
            name='search'
            id='search'
            onChange={this.handleChange}
          ></input>
          <button onClick={this.handleClick} className='btn-warning'>
            Search
          </button>
        </form>
        <ErrorBox msg={this.state.message} />
      </div>
    );
  }
}
