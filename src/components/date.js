import React, { Component } from 'react';

export default class TodayDate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: null,
    };
  }
  getTodayDate = () => {
    let dt = new Date();
    let today = dt.getDate() + '/' + (dt.getMonth() + 1) + '/' + dt.getFullYear();

    this.setState({ date: today });
  };
  componentDidMount() {
    this.getTodayDate();
  }

  render() {
    return <h3 className='temp'>{this.state.date}</h3>;
  }
}
