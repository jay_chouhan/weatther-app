import React, { Component } from 'react';
import Card from './Card';

export default class CardWrapper extends Component {
  render() {
    return (
      <div className='card-wrapper'>
        {this.props.data.map((details, index) => {
          return <Card key={index} getDetails={details} />;
        })}
      </div>
    );
  }
}
