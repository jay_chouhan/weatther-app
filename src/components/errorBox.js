import React, { Component } from 'react';

export default class ErrorBox extends Component {
  render() {
    return <div className='errorMsg'>{this.props.msg && <p>{this.props.msg}</p>}</div>;
  }
}
