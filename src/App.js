import React, { Component } from 'react';

import './App.scss';
import SearchBar from './components/Search';
import CardWrapper from './components/Card-wrapper';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      city: '',
      message: '',
      loading: false,
    };
  }

  getData = (value) => {
    this.setState({ city: value, loading: true });

    fetch(`https://api.openweathermap.org/data/2.5/forecast?q=${value}&appid=4a1fa729eaa96215c8a65322892fc831`)
      .then((res) => res.json())
      .then((data) => {
        this.setState({ loading: false });

        if (data.message === 0) {
          let fetchCity = data.city.name;
          this.setState({ fetchCity });
          let fullReport = this.getWeatherReport(data.list);
          this.setState({ data: fullReport, message: '' });
        } else {
          this.setState({ message: data.message });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  getDay(dt) {
    let date = new Date(dt * 1000);
    let reportDay = date.getDay();
    return reportDay;
  }

  getWeatherReport = (data) => {
    let days = data.map((report) => {
      let { dt } = report;
      let reportDay = this.getDay(dt);
      return reportDay;
    });

    days = [...new Set(days)];

    let structuredWeatherReport = days.map((day, index) => {
      let filteredReport = data.filter((report) => {
        let { dt } = report;
        let reportDay = this.getDay(dt);
        if (reportDay === day) {
          return true;
        }
        return false;
      });

      return {
        day: day,
        report: filteredReport[0],
        showDetails: false,
      };
    });

    return structuredWeatherReport;
  };

  render() {
    return (
      <div className='App'>
        <div className='App-header'>
          <h1>Weather App</h1>
        </div>
        <SearchBar getData={this.getData} msg={this.state.message} />
        <div>
          {this.state.loading && (
            <div>
              <i className='fas fa-spinner'></i>
            </div>
          )}
        </div>
        <div>
          <div>
            <h1>{this.state.fetchCity}</h1>
          </div>
        </div>
        <CardWrapper data={this.state.data} />
      </div>
    );
  }
}

export default App;
